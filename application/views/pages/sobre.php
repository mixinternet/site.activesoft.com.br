<?php
// override the default page view to customize

$this->ui->setPageTitle(__('Sobre'));
?>
<?= \View::factory('components/layout/page-title', ['title' => 'Sobre']) ?>
<div class="container">
  <div class="row">
    <div class="col-md-7">
    <p>A Activesoft Consultoria é uma empresa situada no Brasil, em Natal, Rio Grande do Norte, e desenvolve soluções de gestão Administrativa para instituições de ensino, com mais de dez anos de atuação.</p>

    <p>Por ser especialista, estuda os processos que serão vivenciados pelos clientes e trabalha em parceria para prepará-los para os desafios com soluções simples e eficientes, fornecendo aos seus administradores informações confiáveis e completas que subsidiam sua estratégia de crescimento. </p>

    <p> A Activesoft atende a mais de 150 clientes em vários Estados do    Nordeste, fornecendo consultoria profissional e sistemas direcionados e completos, implantados de forma integrada, compartilhando informações entre si ou em conjunto com sistemas já existentes. </p>

    <p>	Os clientes Activesoft contam com serviços de implantação e treinamento em suas instalações, gerenciamento, manutenção de bancos de dados. E ainda setores especializados de análise, controle de qualidade, implantação, desenvolvimento e atendimento.</p>
    </div>
    <div class="col-md-5">
      <img class="w-100 img-fluid" src="<?= $this->url('assets/img/sobre.png') ?>" alt="">
    </div>
  </div>
</div>
<section class="blog py-5">
  <h2 class="font-weight-bold text-center text-primary">Blog</h2>
  <div class="container">
    <div class="row blog-cards"></div>
  </div>
</section>