<?php
    \Helper\LayoutHelper::setInterna(false);
?>
<img src="http://placehold.it/1920x600" alt="" class="vitrine">
<div class="container py-5">
  <div class="text-center">
    <h2>A empresa líder no mercado do RN e referência no Nordeste em soluções em Gestão Educacional</h2>
    <a href="javascript:;" class="btn btn-wide btn-gradient-blue text-white font-weight-bold mt-3">Quero conhecer</a>
  </div>
</div>
<div class="container">
  <div class="d-flex py-5">
    <div class="nav flex-column nav-pills col-3 features-tab__list" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      <a class="nav-link active" 
        id="v-pills-home-tab" 
        data-toggle="pill" 
        href="#v-pills-home" 
        role="tab" 
        aria-controls="v-pills-home" 
        aria-selected="true">Gestão Acadêmica</a>
      <a class="nav-link" 
          id="v-pills-profile-tab" 
          data-toggle="pill" 
          href="#v-pills-profile" 
          role="tab" 
          aria-controls="v-pills-profile" 
          aria-selected="false">Gestão Financeira</a>
      <a class="nav-link" 
        id="v-pills-messages-tab" 
        data-toggle="pill" 
        href="#v-pills-messages" 
        role="tab" 
        aria-controls="v-pills-messages" 
        aria-selected="false">Sistemas Complementares</a>
    </div>
    <div class="tab-content features-tab col-9" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
        <h3 class="text-secondary font-weight-bold">Tenha uma secretaria completa e organizada</h3>
        <p>Integração e eficiência é a chave do sucesso. Secretaria completa e organizada, 
        Avaliação pedagógica personalizada, Acompanhamento do desempenho do aluno personalizado e Orientação educacional.</p>
        <a href="javascript:;" class="btn btn-wide btn-gradient-orange text-white font-weight-bold mt-3">Conheça o sistema</a>
      </div>
      <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
      <h3 class="text-primary font-weight-bold">Tenha uma secretaria completa e organizada</h3>
        <p>Integração e eficiência é a chave do sucesso. Secretaria completa e organizada, 
        Avaliação pedagógica personalizada, Acompanhamento do desempenho do aluno personalizado e Orientação educacional.</p>
        <a href="javascript:;" class="btn btn-wide btn-gradient-blue text-white font-weight-bold mt-3">Conheça o sistema</a>
      </div>
      <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">

      </div>
    </div>
  </div>
</div>
<section class="blog bg-light py-5">
  <h2 class="font-weight-bold text-center text-primary">Blog</h2>
  <div class="container">
    <div class="row blog-cards"></div>
  </div>
</section>
<section class="bg-primary">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-4">
        <div class="p-5 d-flex align-items-center justify-content-around flex-column text-center">
          <img src="<?= $this->url('assets/img/home-icon/1.png') ?>" alt="" class="img-fluid">
          <h5 class="text-white mt-3">Armazenamento em nuvem</h5>
        </div>
      </div>
      <div class="col-md-4">
        <div class="p-5 d-flex align-items-center justify-content-around flex-column text-center">
          <img src="<?= $this->url('assets/img/home-icon/2.png') ?>" alt="" class="img-fluid">
          <h5 class="text-white mt-3">Segurança garantida</h5>
        </div>
      </div>
      <div class="col-md-4">
        <div class="p-5 d-flex align-items-center justify-content-around flex-column text-center">
          <img src="<?= $this->url('assets/img/home-icon/3.png') ?>" alt="" class="img-fluid">
          <h5 class="text-white mt-3">Implantação de Consultoria</h5>
        </div>
      </div>
      <div class="col-md-4">
        <div class="p-5 d-flex align-items-center justify-content-around flex-column text-center">
          <img src="<?= $this->url('assets/img/home-icon/4.png') ?>" alt="" class="img-fluid">
          <h5 class="text-white mt-3">Suporte 24h</h5>
        </div>
      </div>
      <div class="col-md-4">
        <div class="p-5 d-flex align-items-center justify-content-around flex-column text-center">
          <img src="<?= $this->url('assets/img/home-icon/5.png') ?>" alt="" class="img-fluid">
          <h5 class="text-white mt-3">Certificação</h5>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="numbers">
  <div class="container">
    <div class="text-center font-weight-light text-white mb-5">
      Os números falam por si
    </div>
    <div class="numbers-data row">
      <div class="col">
        <span class="numbers-data__big">
          +de 500
        </span>
        <span class="numbers-data__small">
          instituições cadastradas
        </span>
      </div>
      <div class="col">
        <span class="numbers-data__big">
          5000
        </span>
        <span class="numbers-data__small">
          usuários finais
        </span>
      </div>
      <div class="col">
        <span class="numbers-data__big">
          700
        </span>
        <span class="numbers-data__small">
          cidades
        </span>
      </div>
      <div class="col">
        <span class="numbers-data__big">
          95%
        </span>
        <span class="numbers-data__small">
          satisfação
        </span>
      </div>
    </div>
  </div>
</section>
<section class="clientes">
  <div class="glide quotes">
    <div class="glide__track" data-glide-el="track">
      <div class="glide__slides">
        <div class="glide__slide">
          <div class="quote-card">
            <img class="rounded-circle" src="http://placehold.it/150x150" alt="">
            <p class="pt-4">“O sistema está ajudando bastante, acabou com o método manual que utilizávamos e nosso processo 
            hoje melhorou muito e com isso proporcionou uma rotina diária mais prática e fluida.”</p>
            <a class="d-inline-block" href="javacript:;">Continue lendo</a>
            <span class="d-block">Amanda Tavares - Administradora - Nome da Empresa</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="text-center py-5">
      <h2 class="text-primary font-weight-bold">Clientes</h2>
    </div>
    <div class="clientes-logo glide py-5">
      <div class="glide__track" data-glide-el="track">
        <div class="img-wrapper">
          <img class="img-fluid"src="http://placehold.it/84x62" alt="">
          <img class="img-fluid"src="http://placehold.it/84x62" alt="">
          <img class="img-fluid"src="http://placehold.it/84x62" alt="">
          <img class="img-fluid"src="http://placehold.it/84x62" alt="">
          <img class="img-fluid"src="http://placehold.it/84x62" alt="">
        </div>
      </div>
      <div class="glide__arrows" data-glide-el="controls">
        <button class="glide__arrow glide__arrow--left text-dark border-0" data-glide-dir="<"><i class="fas fa-chevron-left"></i></button>
        <button class="glide__arrow glide__arrow--right text-dark border-0" data-glide-dir=">"><i class="fas fa-chevron-right"></i></button>
      </div>
    </div>
  </div>
</section>
<section class="form-contact text-center py-5">
  <div class="container"> 
    <h2 class="text-primary font-weight-bold">Ficou com alguma dúvida? <small class="d-block font-weight-light text-dark">Podemos entrar em contat com você</small></h2>
    <div class="form-footer py-5">
      <form action="kkk">
        <div class="form-group">
          <div class="form-row">
            <div class="col-6">
              <input type="text" placeholder="Nome" class="form-control rounded-0 border-top-0 border-left-0 border-right-0 border-bottom">
            </div>
            <div class="col-6">
              <input type="text" placeholder="E-mail" class="form-control rounded-0 border-top-0 border-left-0 border-right-0 border-bottom">
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="form-row">
            <div class="col-6">
              <input type="text" placeholder="Empresa" class="form-control rounded-0 border-top-0 border-left-0 border-right-0 border-bottom">
            </div>
            <div class="col-6">
              <input type="text" placeholder="Telefone" class="form-control rounded-0 border-top-0 border-left-0 border-right-0 border-bottom">
            </div>
          </div>
        </div>
        <div class="text-center mt-5">
          <button class="btn btn-primary btn-wide text-white font-weight-bold">Enviar</button>
        </div>
      </form>
    </div>
  </div>
</section>