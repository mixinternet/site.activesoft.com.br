<footer class="footer">
        <div class="bg-primary py-5 text-white">
          <div class="container">
            <div class="row">
              <div class="col-md-2">
                <a class="social-icon" href="javascript:;">
                  <i class="fab fa-facebook-f"></i>
                </a>
                <a class="social-icon" href="javascript:;">
                <i class="fab fa-instagram"></i>
                </a>
              </div>
              <div class="col-md-3">
                <span class="text-white"><i class="fas fa-envelope"></i> contato@activesoft.com.br</span>
              </div>
              <div class="col-md-2">
                <span class="text-white"><i class="fas fa-phone"></i> (84) 4008-9800</span>
              </div>
              <div class="col-md-5">
                <span class="text-white"><i class="fas fa-map-marker"></i> R. Coronel Revoredo Filho, 45, Neópolis, Natal/RN, CEP: 59088-755</span>
              </div>
            </div>
          </div>
        </div>
        <div class="footer__copyright py-2 text-center">
            <a
                href="https://mixinternet.com.br"
                target="_blank"
                rel="noopener"
                title="Desenvolvido por Mix Internet">
                <img
                    src="assets/img/logo-mix-dark.svg"
                    class="footer__copyright--img"
                    alt="Mix Internet">
            </a>
        </div>
    </footer>