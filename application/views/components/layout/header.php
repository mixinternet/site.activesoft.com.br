<nav class="navigation-primary navbar navbar-expand-lg navbar-light font-weight-bold text-dark p-0 <?= !empty($class) ? $class : '' ?>">
    <div class="container">
        <a class="navbar-brand mr-auto" href="<?= $this->routeUrl('home') ?>" title="<?= $this->config('app', 'name') ?>">
            <img class="navigation-primary__logo" src="<?= $this->url('/assets/img/logo.png') ?>" alt="<?= $this->config('app', 'name') ?>">
        </a>

        <?= \View::factory('components/layout/header/navigation') ?>
        <a href="javascript:;" class="btn btn-wide btn-gradient-orange text-white font-weight-bold ml-2 py-3 rounded-0">Área do Cliente</a>
    </div>
</nav>
