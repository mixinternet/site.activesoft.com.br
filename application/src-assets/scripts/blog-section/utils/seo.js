export function postCategories(post) {
  const items = []

  if ('_embedded' in post && post._embedded['wp:term'][0]) {
      const terms = post._embedded['wp:term'][0]

      for (let i = 0; i < terms.length; i++) {
          if (terms[i].name) {
              items.push(terms[i].name)
          }
      }
  }

  return items
}

export function postImage(post) {
  let image = null

  if (post && (((post._embedded || {})['wp:featuredmedia'] || {})[0] || {}).source_url) {
      [image] = post._embedded['wp:featuredmedia']
  }

  return image
}
