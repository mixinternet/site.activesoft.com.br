import Vue from 'vue';
import HTTP from '@/http';
import { postImage } from '@/blog-section/utils/seo';
import BlogCard from '@/blog-section/components/blog-card';

export default {
  init(){
    return new Vue({
      el: '.blog-cards',
      components: {
        'blog-card': BlogCard,
      },
      data: {
        posts: [],
      },
      methods: {
        fetchData(){
          HTTP.get('/wp-json/wp/v2/posts?_embed=true&per_page=3')
          .then(response => {
            this.posts = response.data;
          });
        },
        image(post) {
          return postImage(post)
        },
      },
      mounted(){
        this.fetchData();
      },
      template: `
        <div class="row">
          <div v-for="(post, key) in posts" :key="key" class="col-md-4 my-3">
            <blog-card
              :title="post.title.rendered"
              :text="post.excerpt.rendered"
              :link="post.slug"
              :img="image(post)"
              :alt="post.title.rendered"
            />
          </div>
        </div>`,
    })
  }
}