import axios from 'axios'

/* eslint-disable no-undef */

const HTTP = axios.create({
    baseURL: 'https://www.activesoft.com.br/admin-blog/',
    // baseURL: 'http://blog.mixinternet.com.br/',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    },
})

export default HTTP
