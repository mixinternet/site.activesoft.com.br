import '../styles/index.scss'
import 'bootstrap';
import Glide from '@glidejs/glide';
import BlogSection from '@/blog-section/index';

if(document.querySelector('.clientes-logo')) {
  new Glide('.clientes-logo', {
    type: 'carousel',
    perView: 6,
    focusAt: 'center',
  }).mount()
}

if(document.querySelector('.quotes')) {
  new Glide('.quotes', {
    type: 'carousel',
    perView: 3,
    gap: 250,
    focusAt: 'center',
  }).mount()
}

BlogSection.init();