<?php defined('SYSPATH') OR die('No direct access allowed.');

return [
    [
        'route_name' => 'sobre',
        'route_uri'  => 'sobre',
        'route_parameters' => [],
        'view' => 'pages/sobre',
        'filter' => function(\Mix\Controller\Controller $controller, \View $view) {}
    ]
];
