<?php

use Mix\Router\Router;

return [
  [
      'title' => 'A Activesoft',
      'uri'  => Router::getRouteUrl('pages.show', ['permalink' => 'sobre']),
      'weight' => 10,
      'children' => [
        [
          'title' => 'Exemplo',
          'uri' => 'javascript:;',
        ],
        [
          'title' => 'Exemplo',
          'uri' => 'javascript:;',
        ],
        [
          'title' => 'Exemplo',
          'uri' => 'javascript:;',
        ],
      ]
  ],
  [
      'title' => 'Soluções',
      'uri'  => Router::getRouteUrl('pages.show', ['permalink' => 'sobre']),
      'weight' => 10
  ],
  [
      'title' => 'Cases',
      'uri'  => Router::getRouteUrl('pages.show', ['permalink' => 'sobre']),
      'weight' => 10
  ],
  [
      'title' => 'Materiais',
      'uri'  => Router::getRouteUrl('pages.show', ['permalink' => 'sobre']),
      'weight' => 10
  ],
  [
      'title' => 'Blog',
      'uri'  => Router::getRouteUrl('pages.show', ['permalink' => 'sobre']),
      'weight' => 10
  ],
  [
      'title' => 'Contato',
      'uri'  => Router::getRouteUrl('pages.show', ['permalink' => 'sobre']),
      'weight' => 10
  ],
];
