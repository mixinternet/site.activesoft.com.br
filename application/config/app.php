<?php defined('SYSPATH') OR die('No direct access allowed.');

return [
    'name' => env('APP_NAME', 'Application Name'),
    'version' => env('APP_VERSION', '0.0.1')
];
