/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/assets/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([0,"vendors~main"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader/index.js?!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/lib/index.js?!../../../node_modules/resolve-url-loader/index.js!../../../node_modules/sass-loader/lib/loader.js?!../../../node_modules/vue-loader/lib/index.js?!../scripts/blog-section/components/blog-card.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** C:/MAMP/htdocs/activesoft.com.br/node_modules/mini-css-extract-plugin/dist/loader.js!C:/MAMP/htdocs/activesoft.com.br/node_modules/css-loader??ref--3-1!C:/MAMP/htdocs/activesoft.com.br/node_modules/vue-loader/lib/loaders/stylePostLoader.js!C:/MAMP/htdocs/activesoft.com.br/node_modules/postcss-loader/lib??postcss!C:/MAMP/htdocs/activesoft.com.br/node_modules/resolve-url-loader!C:/MAMP/htdocs/activesoft.com.br/node_modules/sass-loader/lib/loader.js??ref--3-4!C:/MAMP/htdocs/activesoft.com.br/node_modules/vue-loader/lib??vue-loader-options!../scripts/blog-section/components/blog-card.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "../scripts/blog-section/components/blog-card.vue?vue&type=style&index=0&lang=scss&":
/*!******************************************************************************************!*\
  !*** ../scripts/blog-section/components/blog-card.vue?vue&type=style&index=0&lang=scss& ***!
  \******************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module exports are unknown */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_3_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_postcss_node_modules_resolve_url_loader_index_js_node_modules_sass_loader_lib_loader_js_ref_3_4_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_card_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../../../node_modules/css-loader??ref--3-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/lib??postcss!../../../../../node_modules/resolve-url-loader!../../../../../node_modules/sass-loader/lib/loader.js??ref--3-4!../../../../../node_modules/vue-loader/lib??vue-loader-options!./blog-card.vue?vue&type=style&index=0&lang=scss& */ "../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader/index.js?!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/lib/index.js?!../../../node_modules/resolve-url-loader/index.js!../../../node_modules/sass-loader/lib/loader.js?!../../../node_modules/vue-loader/lib/index.js?!../scripts/blog-section/components/blog-card.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_3_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_postcss_node_modules_resolve_url_loader_index_js_node_modules_sass_loader_lib_loader_js_ref_3_4_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_card_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_3_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_postcss_node_modules_resolve_url_loader_index_js_node_modules_sass_loader_lib_loader_js_ref_3_4_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_card_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_3_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_postcss_node_modules_resolve_url_loader_index_js_node_modules_sass_loader_lib_loader_js_ref_3_4_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_card_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_3_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_postcss_node_modules_resolve_url_loader_index_js_node_modules_sass_loader_lib_loader_js_ref_3_4_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_card_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_3_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_postcss_node_modules_resolve_url_loader_index_js_node_modules_sass_loader_lib_loader_js_ref_3_4_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_card_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "../scripts/index.js":
/*!***************************************!*\
  !*** ../scripts/index.js + 8 modules ***!
  \***************************************/
/*! no exports provided */
/*! ModuleConcatenation bailout: Cannot concat with C:/MAMP/htdocs/activesoft.com.br/node_modules/@glidejs/glide/dist/glide.esm.js */
/*! ModuleConcatenation bailout: Cannot concat with C:/MAMP/htdocs/activesoft.com.br/node_modules/axios/index.js (<- Module is not an ECMAScript module) */
/*! ModuleConcatenation bailout: Cannot concat with C:/MAMP/htdocs/activesoft.com.br/node_modules/vue-loader/lib/runtime/componentNormalizer.js */
/*! ModuleConcatenation bailout: Cannot concat with C:/MAMP/htdocs/activesoft.com.br/node_modules/vue/dist/vue.js (<- Module is not an ECMAScript module) */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ../styles/index.scss
var styles = __webpack_require__("../styles/index.scss");

// EXTERNAL MODULE: C:/MAMP/htdocs/activesoft.com.br/node_modules/bootstrap/dist/js/bootstrap.js
var bootstrap = __webpack_require__("../../../node_modules/bootstrap/dist/js/bootstrap.js");

// EXTERNAL MODULE: C:/MAMP/htdocs/activesoft.com.br/node_modules/@glidejs/glide/dist/glide.esm.js
var glide_esm = __webpack_require__("../../../node_modules/@glidejs/glide/dist/glide.esm.js");

// EXTERNAL MODULE: C:/MAMP/htdocs/activesoft.com.br/node_modules/vue/dist/vue.js
var vue = __webpack_require__("../../../node_modules/vue/dist/vue.js");
var vue_default = /*#__PURE__*/__webpack_require__.n(vue);

// EXTERNAL MODULE: C:/MAMP/htdocs/activesoft.com.br/node_modules/axios/index.js
var axios = __webpack_require__("../../../node_modules/axios/index.js");
var axios_default = /*#__PURE__*/__webpack_require__.n(axios);

// CONCATENATED MODULE: ../scripts/http.js


/* eslint-disable no-undef */

const HTTP = axios_default.a.create({
    baseURL: 'https://www.activesoft.com.br/admin-blog/',
    // baseURL: 'http://blog.mixinternet.com.br/',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
});

/* harmony default export */ var http = (HTTP);
// CONCATENATED MODULE: ../scripts/blog-section/utils/seo.js
function postCategories(post) {
    const items = [];

    if ('_embedded' in post && post._embedded['wp:term'][0]) {
        const terms = post._embedded['wp:term'][0];

        for (let i = 0; i < terms.length; i++) {
            if (terms[i].name) {
                items.push(terms[i].name);
            }
        }
    }

    return items;
}

function postImage(post) {
    let image = null;

    if (post && (((post._embedded || {})['wp:featuredmedia'] || {})[0] || {}).source_url) {
        [image] = post._embedded['wp:featuredmedia'];
    }

    return image;
}
// CONCATENATED MODULE: C:/MAMP/htdocs/activesoft.com.br/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!C:/MAMP/htdocs/activesoft.com.br/node_modules/vue-loader/lib??vue-loader-options!../scripts/blog-section/components/blog-card.vue?vue&type=template&id=253260f2&
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "a",
    {
      staticClass: "blog-card text-dark",
      attrs: { href: "/blog/" + _vm.link }
    },
    [
      _c("img", {
        staticClass: "blog-card__image rounded",
        attrs: { src: _vm.imgUrl, alt: _vm.alt }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "blog-card__content" }, [
        _c("h2", {
          staticClass: "blog-card__title",
          domProps: { innerHTML: _vm._s(_vm.title) }
        }),
        _vm._v(" "),
        _c("p", {
          staticClass: "blog-card__text",
          domProps: { innerHTML: _vm._s(_vm.text) }
        })
      ]),
      _vm._v(" "),
      _vm._m(0)
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-right" }, [
      _c("span", { staticClass: "blog-card__link" }, [
        _vm._v("\n    Continue Lendo...\n  ")
      ])
    ])
  }
]
render._withStripped = true


// CONCATENATED MODULE: ../scripts/blog-section/components/blog-card.vue?vue&type=template&id=253260f2&

// CONCATENATED MODULE: C:/MAMP/htdocs/activesoft.com.br/node_modules/babel-loader/lib??ref--1-0!C:/MAMP/htdocs/activesoft.com.br/node_modules/vue-loader/lib??vue-loader-options!../scripts/blog-section/components/blog-card.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var blog_cardvue_type_script_lang_js_ = ({
  name: 'blog-card',
  props: ['title', 'text', 'link', 'alt', 'img'],
  computed: {
    imgUrl() {
      return (this.img || {}).source_url;
    }
  }
});
// CONCATENATED MODULE: ../scripts/blog-section/components/blog-card.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_blog_cardvue_type_script_lang_js_ = (blog_cardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ../scripts/blog-section/components/blog-card.vue?vue&type=style&index=0&lang=scss&
var blog_cardvue_type_style_index_0_lang_scss_ = __webpack_require__("../scripts/blog-section/components/blog-card.vue?vue&type=style&index=0&lang=scss&");

// EXTERNAL MODULE: C:/MAMP/htdocs/activesoft.com.br/node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js");

// CONCATENATED MODULE: ../scripts/blog-section/components/blog-card.vue






/* normalize component */

var component = Object(componentNormalizer["default"])(
  components_blog_cardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "scripts/blog-section/components/blog-card.vue"
/* harmony default export */ var blog_card = (component.exports);
// CONCATENATED MODULE: ../scripts/blog-section/index.js





/* harmony default export */ var blog_section = ({
  init() {
    return new vue_default.a({
      el: '.blog-cards',
      components: {
        'blog-card': blog_card
      },
      data: {
        posts: []
      },
      methods: {
        fetchData() {
          http.get('/wp-json/wp/v2/posts?_embed=true&per_page=3').then(response => {
            this.posts = response.data;
          });
        },
        image(post) {
          return postImage(post);
        }
      },
      mounted() {
        this.fetchData();
      },
      template: `
        <div class="row">
          <div v-for="(post, key) in posts" :key="key" class="col-md-4 my-3">
            <blog-card
              :title="post.title.rendered"
              :text="post.excerpt.rendered"
              :link="post.slug"
              :img="image(post)"
              :alt="post.title.rendered"
            />
          </div>
        </div>`
    });
  }
});
// CONCATENATED MODULE: ../scripts/index.js





if (document.querySelector('.clientes-logo')) {
  new glide_esm["default"]('.clientes-logo', {
    type: 'carousel',
    perView: 6,
    focusAt: 'center'
  }).mount();
}

if (document.querySelector('.quotes')) {
  new glide_esm["default"]('.quotes', {
    type: 'carousel',
    perView: 3,
    gap: 250,
    focusAt: 'center'
  }).mount();
}

blog_section.init();

/***/ }),

/***/ "../styles/index.scss":
/*!****************************!*\
  !*** ../styles/index.scss ***!
  \****************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ 0:
/*!*********************************!*\
  !*** multi ../scripts/index.js ***!
  \*********************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\MAMP\htdocs\activesoft.com.br\application\src-assets\scripts\index.js */"../scripts/index.js");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4uL3NjcmlwdHMvYmxvZy1zZWN0aW9uL2NvbXBvbmVudHMvYmxvZy1jYXJkLnZ1ZT8zNzBhIiwid2VicGFjazovLy8uLi9zY3JpcHRzL2Jsb2ctc2VjdGlvbi9jb21wb25lbnRzL2Jsb2ctY2FyZC52dWU/YjlhZSIsIndlYnBhY2s6Ly8vLi4vc2NyaXB0cy9odHRwLmpzIiwid2VicGFjazovLy8uLi9zY3JpcHRzL2Jsb2ctc2VjdGlvbi91dGlscy9zZW8uanMiLCJ3ZWJwYWNrOi8vLy4uL3NjcmlwdHMvYmxvZy1zZWN0aW9uL2NvbXBvbmVudHMvYmxvZy1jYXJkLnZ1ZT9hZjY2Iiwid2VicGFjazovLy8uLi9zY3JpcHRzL2Jsb2ctc2VjdGlvbi9jb21wb25lbnRzL2Jsb2ctY2FyZC52dWUiLCJ3ZWJwYWNrOi8vLy4uL3NjcmlwdHMvYmxvZy1zZWN0aW9uL2NvbXBvbmVudHMvYmxvZy1jYXJkLnZ1ZT8zOTJmIiwid2VicGFjazovLy8uLi9zY3JpcHRzL2Jsb2ctc2VjdGlvbi9jb21wb25lbnRzL2Jsb2ctY2FyZC52dWU/OGY3NiIsIndlYnBhY2s6Ly8vLi4vc2NyaXB0cy9ibG9nLXNlY3Rpb24vaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4uL3NjcmlwdHMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4uL3N0eWxlcy9pbmRleC5zY3NzIl0sIm5hbWVzIjpbIkhUVFAiLCJheGlvcyIsImNyZWF0ZSIsImJhc2VVUkwiLCJoZWFkZXJzIiwicG9zdENhdGVnb3JpZXMiLCJwb3N0IiwiaXRlbXMiLCJfZW1iZWRkZWQiLCJ0ZXJtcyIsImkiLCJsZW5ndGgiLCJuYW1lIiwicHVzaCIsInBvc3RJbWFnZSIsImltYWdlIiwic291cmNlX3VybCIsImluaXQiLCJWdWUiLCJlbCIsImNvbXBvbmVudHMiLCJCbG9nQ2FyZCIsImRhdGEiLCJwb3N0cyIsIm1ldGhvZHMiLCJmZXRjaERhdGEiLCJnZXQiLCJ0aGVuIiwicmVzcG9uc2UiLCJtb3VudGVkIiwidGVtcGxhdGUiLCJkb2N1bWVudCIsInF1ZXJ5U2VsZWN0b3IiLCJHbGlkZSIsInR5cGUiLCJwZXJWaWV3IiwiZm9jdXNBdCIsIm1vdW50IiwiZ2FwIiwiQmxvZ1NlY3Rpb24iXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGdCQUFRLG9CQUFvQjtBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUFpQiw0QkFBNEI7QUFDN0M7QUFDQTtBQUNBLDBCQUFrQiwyQkFBMkI7QUFDN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esa0RBQTBDLGdDQUFnQztBQUMxRTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGdFQUF3RCxrQkFBa0I7QUFDMUU7QUFDQSx5REFBaUQsY0FBYztBQUMvRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQXlDLGlDQUFpQztBQUMxRSx3SEFBZ0gsbUJBQW1CLEVBQUU7QUFDckk7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUFnQix1QkFBdUI7QUFDdkM7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDdEpBLHVDOzs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBMGdCLENBQWdCLDJlQUFHLEVBQUMsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBOWhCOztBQUVBOztBQUVBLE1BQU1BLE9BQU9DLGVBQUtBLENBQUNDLE1BQU4sQ0FBYTtBQUN0QkMsYUFBUywyQ0FEYTtBQUV0QjtBQUNBQyxhQUFTO0FBQ0wsa0JBQVUsa0JBREw7QUFFTCx3QkFBZ0I7QUFGWDtBQUhhLENBQWIsQ0FBYjs7QUFTZUosNkNBQWYsRTs7QUNiTyxTQUFTSyxjQUFULENBQXdCQyxJQUF4QixFQUE4QjtBQUNuQyxVQUFNQyxRQUFRLEVBQWQ7O0FBRUEsUUFBSSxlQUFlRCxJQUFmLElBQXVCQSxLQUFLRSxTQUFMLENBQWUsU0FBZixFQUEwQixDQUExQixDQUEzQixFQUF5RDtBQUNyRCxjQUFNQyxRQUFRSCxLQUFLRSxTQUFMLENBQWUsU0FBZixFQUEwQixDQUExQixDQUFkOztBQUVBLGFBQUssSUFBSUUsSUFBSSxDQUFiLEVBQWdCQSxJQUFJRCxNQUFNRSxNQUExQixFQUFrQ0QsR0FBbEMsRUFBdUM7QUFDbkMsZ0JBQUlELE1BQU1DLENBQU4sRUFBU0UsSUFBYixFQUFtQjtBQUNmTCxzQkFBTU0sSUFBTixDQUFXSixNQUFNQyxDQUFOLEVBQVNFLElBQXBCO0FBQ0g7QUFDSjtBQUNKOztBQUVELFdBQU9MLEtBQVA7QUFDRDs7QUFFTSxTQUFTTyxTQUFULENBQW1CUixJQUFuQixFQUF5QjtBQUM5QixRQUFJUyxRQUFRLElBQVo7O0FBRUEsUUFBSVQsUUFBUSxDQUFDLENBQUMsQ0FBQ0EsS0FBS0UsU0FBTCxJQUFrQixFQUFuQixFQUF1QixrQkFBdkIsS0FBOEMsRUFBL0MsRUFBbUQsQ0FBbkQsS0FBeUQsRUFBMUQsRUFBOERRLFVBQTFFLEVBQXNGO0FBQ2xGLFNBQUNELEtBQUQsSUFBVVQsS0FBS0UsU0FBTCxDQUFlLGtCQUFmLENBQVY7QUFDSDs7QUFFRCxXQUFPTyxLQUFQO0FBQ0QsQzs7QUN4QkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCLE9BQU87QUFDUDtBQUNBLGlCQUFpQixvQ0FBb0M7QUFDckQ7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLDRCQUE0QjtBQUNsRCxrQkFBa0IsaUNBQWlDO0FBQ25EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBQ0EsbUJBREE7QUFFQSxVQUNBLE9BREEsRUFFQSxNQUZBLEVBR0EsTUFIQSxFQUlBLEtBSkEsRUFLQSxLQUxBLENBRkE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBVEEsRzs7QUNmcU0sQ0FBZ0Isa0hBQUcsRUFBQyxDOzs7Ozs7OztBQ0FqSTtBQUMzQjtBQUNMO0FBQ2M7OztBQUd0RTtBQUNtRztBQUNuRyxnQkFBZ0Isc0NBQVU7QUFDMUIsRUFBRSw0Q0FBTTtBQUNSLEVBQUUsTUFBTTtBQUNSLEVBQUUsZUFBZTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLElBQUksS0FBVSxFQUFFLFlBaUJmO0FBQ0Q7QUFDZSwrRDs7QUN2Q2Y7QUFDQTtBQUNBO0FBQ0E7O0FBRWU7QUFDYkUsU0FBTTtBQUNKLFdBQU8sSUFBSUMsYUFBSixDQUFRO0FBQ2JDLFVBQUksYUFEUztBQUViQyxrQkFBWTtBQUNWLHFCQUFhQyxTQUFRQTtBQURYLE9BRkM7QUFLYkMsWUFBTTtBQUNKQyxlQUFPO0FBREgsT0FMTztBQVFiQyxlQUFTO0FBQ1BDLG9CQUFXO0FBQ1R6QixjQUFJQSxDQUFDMEIsR0FBTCxDQUFTLDZDQUFULEVBQ0NDLElBREQsQ0FDTUMsWUFBWTtBQUNoQixpQkFBS0wsS0FBTCxHQUFhSyxTQUFTTixJQUF0QjtBQUNELFdBSEQ7QUFJRCxTQU5NO0FBT1BQLGNBQU1ULElBQU4sRUFBWTtBQUNWLGlCQUFPUSxTQUFTQSxDQUFDUixJQUFWLENBQVA7QUFDRDtBQVRNLE9BUkk7QUFtQmJ1QixnQkFBUztBQUNQLGFBQUtKLFNBQUw7QUFDRCxPQXJCWTtBQXNCYkssZ0JBQVc7Ozs7Ozs7Ozs7OztBQXRCRSxLQUFSLENBQVA7QUFtQ0Q7QUFyQ1ksQ0FBZixFOztBQ0xBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLElBQUdDLFNBQVNDLGFBQVQsQ0FBdUIsZ0JBQXZCLENBQUgsRUFBNkM7QUFDM0MsTUFBSUMsb0JBQUosQ0FBVSxnQkFBVixFQUE0QjtBQUMxQkMsVUFBTSxVQURvQjtBQUUxQkMsYUFBUyxDQUZpQjtBQUcxQkMsYUFBUztBQUhpQixHQUE1QixFQUlHQyxLQUpIO0FBS0Q7O0FBRUQsSUFBR04sU0FBU0MsYUFBVCxDQUF1QixTQUF2QixDQUFILEVBQXNDO0FBQ3BDLE1BQUlDLG9CQUFKLENBQVUsU0FBVixFQUFxQjtBQUNuQkMsVUFBTSxVQURhO0FBRW5CQyxhQUFTLENBRlU7QUFHbkJHLFNBQUssR0FIYztBQUluQkYsYUFBUztBQUpVLEdBQXJCLEVBS0dDLEtBTEg7QUFNRDs7QUFFREUsWUFBV0EsQ0FBQ3RCLElBQVosRzs7Ozs7Ozs7Ozs7O0FDdEJBLHVDIiwiZmlsZSI6ImpzL2J1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIGluc3RhbGwgYSBKU09OUCBjYWxsYmFjayBmb3IgY2h1bmsgbG9hZGluZ1xuIFx0ZnVuY3Rpb24gd2VicGFja0pzb25wQ2FsbGJhY2soZGF0YSkge1xuIFx0XHR2YXIgY2h1bmtJZHMgPSBkYXRhWzBdO1xuIFx0XHR2YXIgbW9yZU1vZHVsZXMgPSBkYXRhWzFdO1xuIFx0XHR2YXIgZXhlY3V0ZU1vZHVsZXMgPSBkYXRhWzJdO1xuXG4gXHRcdC8vIGFkZCBcIm1vcmVNb2R1bGVzXCIgdG8gdGhlIG1vZHVsZXMgb2JqZWN0LFxuIFx0XHQvLyB0aGVuIGZsYWcgYWxsIFwiY2h1bmtJZHNcIiBhcyBsb2FkZWQgYW5kIGZpcmUgY2FsbGJhY2tcbiBcdFx0dmFyIG1vZHVsZUlkLCBjaHVua0lkLCBpID0gMCwgcmVzb2x2ZXMgPSBbXTtcbiBcdFx0Zm9yKDtpIDwgY2h1bmtJZHMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHRjaHVua0lkID0gY2h1bmtJZHNbaV07XG4gXHRcdFx0aWYoaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdKSB7XG4gXHRcdFx0XHRyZXNvbHZlcy5wdXNoKGluc3RhbGxlZENodW5rc1tjaHVua0lkXVswXSk7XG4gXHRcdFx0fVxuIFx0XHRcdGluc3RhbGxlZENodW5rc1tjaHVua0lkXSA9IDA7XG4gXHRcdH1cbiBcdFx0Zm9yKG1vZHVsZUlkIGluIG1vcmVNb2R1bGVzKSB7XG4gXHRcdFx0aWYoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vcmVNb2R1bGVzLCBtb2R1bGVJZCkpIHtcbiBcdFx0XHRcdG1vZHVsZXNbbW9kdWxlSWRdID0gbW9yZU1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHRcdH1cbiBcdFx0fVxuIFx0XHRpZihwYXJlbnRKc29ucEZ1bmN0aW9uKSBwYXJlbnRKc29ucEZ1bmN0aW9uKGRhdGEpO1xuXG4gXHRcdHdoaWxlKHJlc29sdmVzLmxlbmd0aCkge1xuIFx0XHRcdHJlc29sdmVzLnNoaWZ0KCkoKTtcbiBcdFx0fVxuXG4gXHRcdC8vIGFkZCBlbnRyeSBtb2R1bGVzIGZyb20gbG9hZGVkIGNodW5rIHRvIGRlZmVycmVkIGxpc3RcbiBcdFx0ZGVmZXJyZWRNb2R1bGVzLnB1c2guYXBwbHkoZGVmZXJyZWRNb2R1bGVzLCBleGVjdXRlTW9kdWxlcyB8fCBbXSk7XG5cbiBcdFx0Ly8gcnVuIGRlZmVycmVkIG1vZHVsZXMgd2hlbiBhbGwgY2h1bmtzIHJlYWR5XG4gXHRcdHJldHVybiBjaGVja0RlZmVycmVkTW9kdWxlcygpO1xuIFx0fTtcbiBcdGZ1bmN0aW9uIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCkge1xuIFx0XHR2YXIgcmVzdWx0O1xuIFx0XHRmb3IodmFyIGkgPSAwOyBpIDwgZGVmZXJyZWRNb2R1bGVzLmxlbmd0aDsgaSsrKSB7XG4gXHRcdFx0dmFyIGRlZmVycmVkTW9kdWxlID0gZGVmZXJyZWRNb2R1bGVzW2ldO1xuIFx0XHRcdHZhciBmdWxmaWxsZWQgPSB0cnVlO1xuIFx0XHRcdGZvcih2YXIgaiA9IDE7IGogPCBkZWZlcnJlZE1vZHVsZS5sZW5ndGg7IGorKykge1xuIFx0XHRcdFx0dmFyIGRlcElkID0gZGVmZXJyZWRNb2R1bGVbal07XG4gXHRcdFx0XHRpZihpbnN0YWxsZWRDaHVua3NbZGVwSWRdICE9PSAwKSBmdWxmaWxsZWQgPSBmYWxzZTtcbiBcdFx0XHR9XG4gXHRcdFx0aWYoZnVsZmlsbGVkKSB7XG4gXHRcdFx0XHRkZWZlcnJlZE1vZHVsZXMuc3BsaWNlKGktLSwgMSk7XG4gXHRcdFx0XHRyZXN1bHQgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IGRlZmVycmVkTW9kdWxlWzBdKTtcbiBcdFx0XHR9XG4gXHRcdH1cbiBcdFx0cmV0dXJuIHJlc3VsdDtcbiBcdH1cblxuIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gb2JqZWN0IHRvIHN0b3JlIGxvYWRlZCBhbmQgbG9hZGluZyBjaHVua3NcbiBcdC8vIHVuZGVmaW5lZCA9IGNodW5rIG5vdCBsb2FkZWQsIG51bGwgPSBjaHVuayBwcmVsb2FkZWQvcHJlZmV0Y2hlZFxuIFx0Ly8gUHJvbWlzZSA9IGNodW5rIGxvYWRpbmcsIDAgPSBjaHVuayBsb2FkZWRcbiBcdHZhciBpbnN0YWxsZWRDaHVua3MgPSB7XG4gXHRcdFwibWFpblwiOiAwXG4gXHR9O1xuXG4gXHR2YXIgZGVmZXJyZWRNb2R1bGVzID0gW107XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiL2Fzc2V0cy9cIjtcblxuIFx0dmFyIGpzb25wQXJyYXkgPSB3aW5kb3dbXCJ3ZWJwYWNrSnNvbnBcIl0gPSB3aW5kb3dbXCJ3ZWJwYWNrSnNvbnBcIl0gfHwgW107XG4gXHR2YXIgb2xkSnNvbnBGdW5jdGlvbiA9IGpzb25wQXJyYXkucHVzaC5iaW5kKGpzb25wQXJyYXkpO1xuIFx0anNvbnBBcnJheS5wdXNoID0gd2VicGFja0pzb25wQ2FsbGJhY2s7XG4gXHRqc29ucEFycmF5ID0ganNvbnBBcnJheS5zbGljZSgpO1xuIFx0Zm9yKHZhciBpID0gMDsgaSA8IGpzb25wQXJyYXkubGVuZ3RoOyBpKyspIHdlYnBhY2tKc29ucENhbGxiYWNrKGpzb25wQXJyYXlbaV0pO1xuIFx0dmFyIHBhcmVudEpzb25wRnVuY3Rpb24gPSBvbGRKc29ucEZ1bmN0aW9uO1xuXG5cbiBcdC8vIGFkZCBlbnRyeSBtb2R1bGUgdG8gZGVmZXJyZWQgbGlzdFxuIFx0ZGVmZXJyZWRNb2R1bGVzLnB1c2goWzAsXCJ2ZW5kb3Jzfm1haW5cIl0pO1xuIFx0Ly8gcnVuIGRlZmVycmVkIG1vZHVsZXMgd2hlbiByZWFkeVxuIFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4iLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iLCJpbXBvcnQgbW9kIGZyb20gXCItIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9taW5pLWNzcy1leHRyYWN0LXBsdWdpbi9kaXN0L2xvYWRlci5qcyEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0zLTEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2xvYWRlcnMvc3R5bGVQb3N0TG9hZGVyLmpzIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3Bvc3Rjc3MhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Jlc29sdmUtdXJsLWxvYWRlci9pbmRleC5qcyEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcz8/cmVmLS0zLTQhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9ibG9nLWNhcmQudnVlP3Z1ZSZ0eXBlPXN0eWxlJmluZGV4PTAmbGFuZz1zY3NzJlwiOyBleHBvcnQgZGVmYXVsdCBtb2Q7IGV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9taW5pLWNzcy1leHRyYWN0LXBsdWdpbi9kaXN0L2xvYWRlci5qcyEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0zLTEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2xvYWRlcnMvc3R5bGVQb3N0TG9hZGVyLmpzIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3Bvc3Rjc3MhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Jlc29sdmUtdXJsLWxvYWRlci9pbmRleC5qcyEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcz8/cmVmLS0zLTQhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9ibG9nLWNhcmQudnVlP3Z1ZSZ0eXBlPXN0eWxlJmluZGV4PTAmbGFuZz1zY3NzJlwiIiwiaW1wb3J0IGF4aW9zIGZyb20gJ2F4aW9zJ1xuXG4vKiBlc2xpbnQtZGlzYWJsZSBuby11bmRlZiAqL1xuXG5jb25zdCBIVFRQID0gYXhpb3MuY3JlYXRlKHtcbiAgICBiYXNlVVJMOiAnaHR0cHM6Ly93d3cuYWN0aXZlc29mdC5jb20uYnIvYWRtaW4tYmxvZy8nLFxuICAgIC8vIGJhc2VVUkw6ICdodHRwOi8vYmxvZy5taXhpbnRlcm5ldC5jb20uYnIvJyxcbiAgICBoZWFkZXJzOiB7XG4gICAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSxcbn0pXG5cbmV4cG9ydCBkZWZhdWx0IEhUVFBcbiIsImV4cG9ydCBmdW5jdGlvbiBwb3N0Q2F0ZWdvcmllcyhwb3N0KSB7XG4gIGNvbnN0IGl0ZW1zID0gW11cblxuICBpZiAoJ19lbWJlZGRlZCcgaW4gcG9zdCAmJiBwb3N0Ll9lbWJlZGRlZFsnd3A6dGVybSddWzBdKSB7XG4gICAgICBjb25zdCB0ZXJtcyA9IHBvc3QuX2VtYmVkZGVkWyd3cDp0ZXJtJ11bMF1cblxuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0ZXJtcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgIGlmICh0ZXJtc1tpXS5uYW1lKSB7XG4gICAgICAgICAgICAgIGl0ZW1zLnB1c2godGVybXNbaV0ubmFtZSlcbiAgICAgICAgICB9XG4gICAgICB9XG4gIH1cblxuICByZXR1cm4gaXRlbXNcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHBvc3RJbWFnZShwb3N0KSB7XG4gIGxldCBpbWFnZSA9IG51bGxcblxuICBpZiAocG9zdCAmJiAoKChwb3N0Ll9lbWJlZGRlZCB8fCB7fSlbJ3dwOmZlYXR1cmVkbWVkaWEnXSB8fCB7fSlbMF0gfHwge30pLnNvdXJjZV91cmwpIHtcbiAgICAgIFtpbWFnZV0gPSBwb3N0Ll9lbWJlZGRlZFsnd3A6ZmVhdHVyZWRtZWRpYSddXG4gIH1cblxuICByZXR1cm4gaW1hZ2Vcbn1cbiIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXG4gICAgXCJhXCIsXG4gICAge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiYmxvZy1jYXJkIHRleHQtZGFya1wiLFxuICAgICAgYXR0cnM6IHsgaHJlZjogXCIvYmxvZy9cIiArIF92bS5saW5rIH1cbiAgICB9LFxuICAgIFtcbiAgICAgIF9jKFwiaW1nXCIsIHtcbiAgICAgICAgc3RhdGljQ2xhc3M6IFwiYmxvZy1jYXJkX19pbWFnZSByb3VuZGVkXCIsXG4gICAgICAgIGF0dHJzOiB7IHNyYzogX3ZtLmltZ1VybCwgYWx0OiBfdm0uYWx0IH1cbiAgICAgIH0pLFxuICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiYmxvZy1jYXJkX19jb250ZW50XCIgfSwgW1xuICAgICAgICBfYyhcImgyXCIsIHtcbiAgICAgICAgICBzdGF0aWNDbGFzczogXCJibG9nLWNhcmRfX3RpdGxlXCIsXG4gICAgICAgICAgZG9tUHJvcHM6IHsgaW5uZXJIVE1MOiBfdm0uX3MoX3ZtLnRpdGxlKSB9XG4gICAgICAgIH0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcInBcIiwge1xuICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJsb2ctY2FyZF9fdGV4dFwiLFxuICAgICAgICAgIGRvbVByb3BzOiB7IGlubmVySFRNTDogX3ZtLl9zKF92bS50ZXh0KSB9XG4gICAgICAgIH0pXG4gICAgICBdKSxcbiAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICBfdm0uX20oMClcbiAgICBdXG4gIClcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwidGV4dC1yaWdodFwiIH0sIFtcbiAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImJsb2ctY2FyZF9fbGlua1wiIH0sIFtcbiAgICAgICAgX3ZtLl92KFwiXFxuICAgIENvbnRpbnVlIExlbmRvLi4uXFxuICBcIilcbiAgICAgIF0pXG4gICAgXSlcbiAgfVxuXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCI8dGVtcGxhdGU+XG4gIDxhIDpocmVmPVwiYC9ibG9nLyR7bGlua31gXCIgY2xhc3M9XCJibG9nLWNhcmQgdGV4dC1kYXJrXCI+XG4gICAgPGltZyBjbGFzcz1cImJsb2ctY2FyZF9faW1hZ2Ugcm91bmRlZFwiIDpzcmM9XCJpbWdVcmxcIiA6YWx0PVwiYWx0XCI+XG4gICAgPGRpdiBjbGFzcz1cImJsb2ctY2FyZF9fY29udGVudFwiPlxuICAgICAgPGgyIHYtaHRtbD1cInRpdGxlXCIgY2xhc3M9XCJibG9nLWNhcmRfX3RpdGxlXCI+PC9oMj5cbiAgICAgIDxwIHYtaHRtbD1cInRleHRcIiBjbGFzcz0nYmxvZy1jYXJkX190ZXh0Jz48L3A+XG4gICAgPC9kaXY+XG4gICAgPGRpdiBjbGFzcz1cInRleHQtcmlnaHRcIj5cbiAgICA8c3BhbiBjbGFzcz1cImJsb2ctY2FyZF9fbGlua1wiPlxuICAgICAgQ29udGludWUgTGVuZG8uLi5cbiAgICA8L3NwYW4+XG4gICAgPC9kaXY+XG4gIDwvYT5cbjwvdGVtcGxhdGU+XG48c2NyaXB0PlxuICBleHBvcnQgZGVmYXVsdCB7XG4gICAgbmFtZTogJ2Jsb2ctY2FyZCcsXG4gICAgcHJvcHM6IFtcbiAgICAgICd0aXRsZScsXG4gICAgICAndGV4dCcsXG4gICAgICAnbGluaycsXG4gICAgICAnYWx0JyxcbiAgICAgICdpbWcnXG4gICAgXSxcbiAgICBjb21wdXRlZDoge1xuICAgICAgaW1nVXJsKCl7XG4gICAgICAgIHJldHVybiAodGhpcy5pbWcgfHwge30pLnNvdXJjZV91cmxcbiAgICAgIH1cbiAgICB9XG4gIH1cbjwvc2NyaXB0PlxuXG48c3R5bGUgbGFuZz1cInNjc3NcIj5cbiAgLmJsb2ctY2FyZCB7XG4gICAgcGFkZGluZzogMTVweDtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICB0cmFuc2l0aW9uOiAzMDBtcztcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGZvbnQtZmFtaWx5OiAnUFQgU2VyaWYnLCBzZXJpZjtcblxuICAgIC5saW5rLW1vcmUge1xuICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG5cbiAgICAmOmhvdmVyIHtcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgIHRyYW5zaXRpb246IDMwMG1zO1xuICAgIH1cblxuICAgICZfX2ltYWdlIHtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgIH1cblxuICAgICZfX3RpdGxlIHtcbiAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA4MDA7XG4gICAgICBjb2xvcjogIzU1NTU1NTtcbiAgICAgIG1hcmdpbjogMTVweCAwO1xuICAgICAgXG4gICAgfVxuXG4gICAgJl9fdGV4dCB7XG4gICAgICBmb250LXN0eWxlOiBpdGFsaWM7XG4gICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICBmb250LXdlaWdodDogMzAwO1xuICAgICAgbGluZS1oZWlnaHQ6IDEuNDQ7XG4gICAgICBjb2xvcjogIzIxMjEyMTtcbiAgICB9XG5cbiAgICAmX19saW5rIHtcbiAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA4MDA7XG4gICAgICBjb2xvcjogIzAwMDAwMDtcblxuICAgICAgJjo6YmVmb3JlIHtcbiAgICAgICAgY29udGVudDogJyc7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgbGVmdDogMDtcbiAgICAgICAgYm90dG9tOiAtMTVweDtcbiAgICAgICAgd2lkdGg6IDUwJTtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIGhlaWdodDogMXB4O1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjViNWI1O1xuICAgICAgfVxuXG4gICAgICAmOmhvdmVyIHtcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG4gICAgICB9XG4gICAgfVxuICB9XG48L3N0eWxlPlxuXG4iLCJpbXBvcnQgbW9kIGZyb20gXCItIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMCEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL2Jsb2ctY2FyZC52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCI7IGV4cG9ydCBkZWZhdWx0IG1vZDsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0wIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vYmxvZy1jYXJkLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImltcG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0gZnJvbSBcIi4vYmxvZy1jYXJkLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0yNTMyNjBmMiZcIlxuaW1wb3J0IHNjcmlwdCBmcm9tIFwiLi9ibG9nLWNhcmQudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9ibG9nLWNhcmQudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5pbXBvcnQgc3R5bGUwIGZyb20gXCIuL2Jsb2ctY2FyZC52dWU/dnVlJnR5cGU9c3R5bGUmaW5kZXg9MCZsYW5nPXNjc3MmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIkM6XFxcXE1BTVBcXFxcaHRkb2NzXFxcXGFjdGl2ZXNvZnQuY29tLmJyXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCcyNTMyNjBmMicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCcyNTMyNjBmMicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vYmxvZy1jYXJkLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0yNTMyNjBmMiZcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgYXBpLnJlcmVuZGVyKCcyNTMyNjBmMicsIHtcbiAgICAgICAgcmVuZGVyOiByZW5kZXIsXG4gICAgICAgIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zXG4gICAgICB9KVxuICAgIH0pXG4gIH1cbn1cbmNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwic2NyaXB0cy9ibG9nLXNlY3Rpb24vY29tcG9uZW50cy9ibG9nLWNhcmQudnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xuaW1wb3J0IEhUVFAgZnJvbSAnQC9odHRwJztcbmltcG9ydCB7IHBvc3RJbWFnZSB9IGZyb20gJ0AvYmxvZy1zZWN0aW9uL3V0aWxzL3Nlbyc7XG5pbXBvcnQgQmxvZ0NhcmQgZnJvbSAnQC9ibG9nLXNlY3Rpb24vY29tcG9uZW50cy9ibG9nLWNhcmQnO1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIGluaXQoKXtcbiAgICByZXR1cm4gbmV3IFZ1ZSh7XG4gICAgICBlbDogJy5ibG9nLWNhcmRzJyxcbiAgICAgIGNvbXBvbmVudHM6IHtcbiAgICAgICAgJ2Jsb2ctY2FyZCc6IEJsb2dDYXJkLFxuICAgICAgfSxcbiAgICAgIGRhdGE6IHtcbiAgICAgICAgcG9zdHM6IFtdLFxuICAgICAgfSxcbiAgICAgIG1ldGhvZHM6IHtcbiAgICAgICAgZmV0Y2hEYXRhKCl7XG4gICAgICAgICAgSFRUUC5nZXQoJy93cC1qc29uL3dwL3YyL3Bvc3RzP19lbWJlZD10cnVlJnBlcl9wYWdlPTMnKVxuICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgIHRoaXMucG9zdHMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuICAgICAgICBpbWFnZShwb3N0KSB7XG4gICAgICAgICAgcmV0dXJuIHBvc3RJbWFnZShwb3N0KVxuICAgICAgICB9LFxuICAgICAgfSxcbiAgICAgIG1vdW50ZWQoKXtcbiAgICAgICAgdGhpcy5mZXRjaERhdGEoKTtcbiAgICAgIH0sXG4gICAgICB0ZW1wbGF0ZTogYFxuICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XG4gICAgICAgICAgPGRpdiB2LWZvcj1cIihwb3N0LCBrZXkpIGluIHBvc3RzXCIgOmtleT1cImtleVwiIGNsYXNzPVwiY29sLW1kLTQgbXktM1wiPlxuICAgICAgICAgICAgPGJsb2ctY2FyZFxuICAgICAgICAgICAgICA6dGl0bGU9XCJwb3N0LnRpdGxlLnJlbmRlcmVkXCJcbiAgICAgICAgICAgICAgOnRleHQ9XCJwb3N0LmV4Y2VycHQucmVuZGVyZWRcIlxuICAgICAgICAgICAgICA6bGluaz1cInBvc3Quc2x1Z1wiXG4gICAgICAgICAgICAgIDppbWc9XCJpbWFnZShwb3N0KVwiXG4gICAgICAgICAgICAgIDphbHQ9XCJwb3N0LnRpdGxlLnJlbmRlcmVkXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PmAsXG4gICAgfSlcbiAgfVxufSIsImltcG9ydCAnLi4vc3R5bGVzL2luZGV4LnNjc3MnXG5pbXBvcnQgJ2Jvb3RzdHJhcCc7XG5pbXBvcnQgR2xpZGUgZnJvbSAnQGdsaWRlanMvZ2xpZGUnO1xuaW1wb3J0IEJsb2dTZWN0aW9uIGZyb20gJ0AvYmxvZy1zZWN0aW9uL2luZGV4JztcblxuaWYoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNsaWVudGVzLWxvZ28nKSkge1xuICBuZXcgR2xpZGUoJy5jbGllbnRlcy1sb2dvJywge1xuICAgIHR5cGU6ICdjYXJvdXNlbCcsXG4gICAgcGVyVmlldzogNixcbiAgICBmb2N1c0F0OiAnY2VudGVyJyxcbiAgfSkubW91bnQoKVxufVxuXG5pZihkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucXVvdGVzJykpIHtcbiAgbmV3IEdsaWRlKCcucXVvdGVzJywge1xuICAgIHR5cGU6ICdjYXJvdXNlbCcsXG4gICAgcGVyVmlldzogMyxcbiAgICBnYXA6IDI1MCxcbiAgICBmb2N1c0F0OiAnY2VudGVyJyxcbiAgfSkubW91bnQoKVxufVxuXG5CbG9nU2VjdGlvbi5pbml0KCk7IiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIl0sInNvdXJjZVJvb3QiOiIifQ==