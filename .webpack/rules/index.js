module.exports = [
    require('./vue'),
    require('./js'),
    require('./images'),
    require('./styles'),
    require('./fonts'),
]
