// ------------------
// @Table of Contents
// ------------------

/**
 * + @Loading Dependencies
 * + @Common Loaders
 * + @Exporting Module
 */


// ---------------------
// @Loading Dependencies
// ---------------------

const
    manifest = require('../manifest'),
    path     = require('path'),
    postcssPresetEnv = require('postcss-preset-env'),
    MiniCssExtractPlugin = require('mini-css-extract-plugin')

// ---------------
// @Common Loaders
// ---------------

const loaders = [
    //'vue-style-loader',
    MiniCssExtractPlugin.loader,
    {
        loader  : 'css-loader',
        options : {
            sourceMap : manifest.IS_DEVELOPMENT,
            minimize  : manifest.IS_PRODUCTION
        },
    },
    {
        loader  : 'postcss-loader',
        options : {
            ident: 'postcss',
            sourceMap : manifest.IS_DEVELOPMENT,
            plugins   : () => [
                postcssPresetEnv(),
            ],
        },
    },
    'resolve-url-loader',
    {
        loader  : 'sass-loader',
        options : {
            workerParallelJobs : 2,
            sourceMap          : manifest.IS_DEVELOPMENT,
            includePaths       : [
                path.join('../../', 'node_modules'),
                path.join(manifest.paths.src, 'assets', 'styles'),
                path.join(manifest.paths.src, ''),
            ],
        },
    },
]

const rule = {
    test: /\.(sa|sc|c)ss$/,
    use  : [].concat(loaders),
}

// -----------------
// @Exporting Module
// -----------------

module.exports = rule
