const
    manifest = require('../manifest')

const plugins = []

plugins.push(
    require('./vuePlugin'),
    require('./imageminPlugin'),
    require('./faviconPlugin'),
    ...(require('./internal')),
    require('./htmlPlugin'),
    require('./caseSensitivePlugin'),
    require('./extractPlugin'),
    require('./copyPlugin')
)

if (manifest.IS_DEVELOPMENT) {
    plugins.push(require('./dashboardPlugin'))
}

if (process.env.BUNDLE_ANALYZER) {
    plugins.push(require('./bundleAnalyzerPlugin'))
}

module.exports = plugins
