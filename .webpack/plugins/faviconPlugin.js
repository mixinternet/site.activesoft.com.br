const
    path                  = require('path'),
    manifest              = require('../manifest'),
    FaviconsWebpackPlugin = require('favicons-webpack-plugin')

module.exports = new FaviconsWebpackPlugin({
    title: 'Bordados Afrodite',
    logo: path.join(manifest.paths.src, 'img', 'logo-mix-dark.svg'),
    background: '#fff',
    prefix: 'img/icons-[hash]/',
})
