const
    path     = require('path'),
    manifest = require('../manifest')
    CopyWebpackPlugin = require('copy-webpack-plugin'),
    paths = []

paths.push(
    ...require('./copy/app'),
    ...require('./copy/scripts_admin'),
    ...require('./copy/scripts_plugins'),
    ...require('./copy/styles_admin'),
    ...require('./copy/styles_plugins'),
    ...require('./copy/images_admin'),
    ...require('./copy/fonts_admin'),
)

module.exports = new CopyWebpackPlugin(
    paths,
    {
        ignore: [ '*.db' ]
    }
)
