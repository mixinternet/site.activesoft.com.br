const
    path     = require('path'),
    manifest = require('../../manifest'),
    paths    = []

paths.push(
    {
        from : path.join(manifest.paths.admin.src, 'fonts'),
        to   : path.join(manifest.paths.admin.css.dist),
    },
    {
        from : path.join(manifest.paths.vendors, 'font-awesome/fonts'),
        to   : path.join(manifest.paths.admin.css.dist, 'fonts'),
    }
)

module.exports = paths
