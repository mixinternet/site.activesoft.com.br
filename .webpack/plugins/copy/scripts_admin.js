const
    path     = require('path'),
    glob     = require('glob'),
    manifest = require('../../manifest'),
    paths    = []

const
    resolvePaths = filename => paths.push(
    {
        from : path.join(manifest.paths.admin.js.src, filename),
        to   : path.join(manifest.paths.admin.js.dist),
    }
)

const scripts = [
    'main.js',
    'login.js',
    'jquery.mask.min.js',
]

scripts.forEach(filename => resolvePaths(filename))

// modules
const files = glob.sync(
    path.join(manifest.paths.modules, '*', 'src-assets', 'js', 'admin', '*.js'),
    {
        cwd : __dirname,
        ignore : './node_modules/**'
    }
)

files.forEach(filename => {
    let moduleName = filename.split('modules').pop().split('src-assets').shift()

    paths.push(
        {
            from : filename,
            to   : path.join(manifest.paths.admin.js.dist, 'modules', moduleName),
        }
    )
})

module.exports = paths
