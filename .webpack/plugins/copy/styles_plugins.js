const
    path     = require('path'),
    manifest = require('../../manifest'),
    paths    = []

const
    resolvePaths = filename => paths.push(
    {
        from : path.join(manifest.paths.admin.plugin.src, filename),
        to   : path.join(manifest.paths.admin.css.dist, 'plugins'),
    }
)

const styles = [
    'bootstrap/css/bootstrap.min.css',
    'perfect-scrollbar/src/perfect-scrollbar.css',
    'bootstrap-datepicker/css/datepicker.css',
    'bootstrap-datetimepicker/css/datetimepicker.css',
    'bootstrap-fileupload/bootstrap-fileupload.min.css',
    'form-validation/css/formValidation.css',
    'select2/css/select2.min.css',
    'sweetalert/sweetalert.css',
    'spectrum/spectrum.css',
    'bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
    'iziModal/css/iziModal.min.css',
    'fullcalendar/fullcalendar/fullcalendar.css',
    'jquery-toggles/css/toggles.css',
    'jquery-toggles/css/themes/toggles-all.css',
    'jQuery-File-Upload/css/jquery.fileupload.css',
    'jQuery-File-Upload/css/jquery.fileupload-noscript.css',
    'jQuery-File-Upload/css/jquery.fileupload-ui-noscript.css',
    'bootstrap-colorpalette/css/bootstrap-colorpalette.css'
]

styles.forEach(filename => resolvePaths(filename))

// extra
paths.push(
    {
        from : path.join(manifest.paths.vendors, 'font-awesome/css/font-awesome.min.css'),
        to   : path.join(manifest.paths.admin.css.dist, 'plugins'),
    }
)

module.exports = paths
