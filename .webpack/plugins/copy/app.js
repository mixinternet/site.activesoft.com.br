const
    path     = require('path'),
    manifest = require('../../manifest')

const paths = [
    {
        from : path.join(manifest.paths.src, 'img'),
        to   : path.join(manifest.paths.build, 'img'),
    }
]

module.exports = paths
