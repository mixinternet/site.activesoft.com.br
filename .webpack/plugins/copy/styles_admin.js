const
    path     = require('path'),
    glob     = require('glob'),
    manifest = require('../../manifest'),
    paths    = []

const
    resolvePaths = filename => paths.push(
    {
        from : path.join(manifest.paths.admin.css.src, filename),
        to   : path.join(manifest.paths.admin.css.dist),
    }
)

const styles = [
    'main.css',
    'main-responsive.css',
    'theme_light.css',
    'print.css'
]

styles.forEach(filename => resolvePaths(filename))

// extra
paths.push(
    {
        from : path.join(manifest.paths.admin.font.src, 'style.css'),
        to   : path.join(manifest.paths.admin.css.dist),
    }
)

// modules
const files = glob.sync(
    path.join(manifest.paths.modules, '*', 'src-assets', 'css', 'admin', '*.css'),
    {
        cwd : __dirname,
        ignore : './node_modules/**'
    }
)

files.forEach(filename => {
    let moduleName = filename.split('modules').pop().split('src-assets').shift()

    paths.push(
        {
            from : filename,
            to   : path.join(manifest.paths.admin.css.dist, 'modules', moduleName),
        }
    )
})

module.exports = paths
