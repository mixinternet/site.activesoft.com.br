const
    path     = require('path'),
    manifest = require('../../manifest'),
    paths    = []

paths.push(
    {
        from : path.join(manifest.paths.admin.src, 'images'),
        to   : path.join(manifest.paths.admin.css.dist, 'images'),
    }
)

module.exports = paths
