const
    path     = require('path'),
    manifest = require('../../manifest'),
    paths    = []

const
    resolvePaths = filename => paths.push(
    {
        from : path.join(manifest.paths.admin.plugin.src, filename),
        to   : path.join(manifest.paths.admin.js.dist, 'plugins'),
    }
)

const plugins = [
    // < IE 9
    'respond.min.js',
    'excanvas.min.js',
    'jQuery-lib/1.10.2/jquery-1102.min.js',

    // > IE 9
    'jQuery-lib/2.0.3/jquery-203.min.js',
    'jquery-ui/jquery-ui-1.10.2.custom.min.js',
    'bootstrap/js/bootstrap.min.js',
    'bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
    'blockUI/jquery.blockUI.js',
    'iCheck/jquery.icheck.min.js',
    'perfect-scrollbar/src/jquery.mousewheel.js',
    'perfect-scrollbar/src/perfect-scrollbar.js',
    'less/less-1.5.0.min.js',
    'jquery-validation/dist/jquery.validate.min.js',
    'bootstrap-datepicker/js/bootstrap-datepicker.js',
    'bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js',
    'bootstrap-datetimepicker/js/bootstrap-datetimepicker.js',
    'bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.pt-BR.js',
    'form-validation/js/formValidation.min.js',
    'form-validation/js/framework/form-validation.bootstrap.min.js',
    'form-validation/js/language/pt_BR.js',
    'select2/js/select2.min.js',
    'select2/js/i18n/pt-BR.js',
    'bootstrap-fileupload/bootstrap-fileupload.min.js',
    'jQuery-File-Upload/js/pacote/tmpl.min.js',
    'jQuery-File-Upload/js/pacote/load-image.all.min.js',
    'jQuery-File-Upload/js/jquery.fileupload.js',
    'jQuery-File-Upload/js/jquery.iframe-transport.js',
    'jQuery-File-Upload/js/jquery.fileupload-process.js',
    'jQuery-File-Upload/js/jquery.fileupload-image.js',
    'sweetalert/sweetalert.min.js',
    'knockout-3.4.1.js',
    'spectrum/spectrum.js',
    'bootstrap-switch/dist/js/bootstrap-switch.min.js',
    'vue.min.js',
    'clipboard.min.js',
    'iziModal/js/iziModal.min.js',
    'jquery-toggles/toggles.min.js',
    'Sortable/Sortable.min.js',
    'moment-with-locales.min.js'
]

plugins.forEach(filename => resolvePaths(filename))

// extra
paths.push(
    {
        from : path.join(manifest.paths.admin.plugin.src, 'kcfinder/'),
        to   : path.join(manifest.paths.admin.js.dist, 'plugins', 'kcfinder'),
    },
    {
        from : path.join(manifest.paths.admin.plugin.src, 'ckeditor/'),
        to   : path.join(manifest.paths.admin.js.dist, 'plugins', 'ckeditor'),
    }
)

module.exports = paths
